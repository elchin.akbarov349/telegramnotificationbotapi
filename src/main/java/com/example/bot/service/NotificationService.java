package com.example.bot.service;

import com.example.bot.dto.NotificationResponse;
import com.example.bot.dto.NotificationRequest;

public interface NotificationService {

    NotificationResponse sendNotification(NotificationRequest notificationRequest);
}
