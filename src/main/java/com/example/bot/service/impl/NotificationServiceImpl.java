package com.example.bot.service.impl;

import com.example.bot.converter.MessageConverter;
import com.example.bot.dto.NotificationRequest;
import com.example.bot.dto.NotificationResponse;
import com.example.bot.notification.NotificationBot;
import com.example.bot.service.NotificationService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class NotificationServiceImpl implements NotificationService {

    private final NotificationBot notificationBot;
    private final MessageConverter messageConverter;

    @Override
    public NotificationResponse sendNotification(NotificationRequest notificationRequest) {
        return notificationBot.sendNotification(messageConverter.getMessage(notificationRequest));
    }
}
