package com.example.bot.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class NotificationRequest implements Serializable {
    private String code;
    private String content;
    private String message;
}
