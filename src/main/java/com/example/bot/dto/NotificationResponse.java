package com.example.bot.dto;

import lombok.Data;

@Data
public class NotificationResponse {
    private String statusCode;
    private String message;
}
