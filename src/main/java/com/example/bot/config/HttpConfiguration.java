package com.example.bot.config;

import org.springframework.context.annotation.Configuration;

import javax.ws.rs.core.UriBuilder;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;

import static com.example.bot.constant.TelegramConstant.*;

@Configuration
public class HttpConfiguration {

    public HttpResponse<String> send(String message, String token, String chatId) {
        try {
            HttpClient client = getClient();
            UriBuilder builder = getUriBuilder(chatId, message);
            HttpRequest httpRequest = HttpRequest.newBuilder()
                    .GET()
                    .uri(builder.build("bot" + token))
                    .timeout(Duration.ofSeconds(5))
                    .build();
            return client.send(httpRequest, HttpResponse.BodyHandlers.ofString());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private HttpClient getClient() {
        return HttpClient.newBuilder()
                .connectTimeout(Duration.ofSeconds(5))
                .version(HttpClient.Version.HTTP_2)
                .build();
    }

    private UriBuilder getUriBuilder(String chatId, String message) {
        return UriBuilder
                .fromUri(TELEGRAM_URL)
                .path(URL_PATH)
                .queryParam(CHAT_ID, chatId)
                .queryParam(TEXT, message);
    }
}
