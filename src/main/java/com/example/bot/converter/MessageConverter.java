package com.example.bot.converter;

import com.example.bot.dto.NotificationRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class MessageConverter {
    private final ObjectMapper objectMapper;
    public String getMessage(NotificationRequest notificationRequest) {
        return notificationRequest.toString();
    }
}
