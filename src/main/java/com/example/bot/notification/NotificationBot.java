package com.example.bot.notification;

import com.example.bot.config.HttpConfiguration;
import com.example.bot.config.TelegramConfiguration;
import com.example.bot.dto.NotificationResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.net.http.HttpResponse;

@Slf4j
@Service
@RequiredArgsConstructor
public class NotificationBot extends TelegramLongPollingBot {

    private final TelegramConfiguration telegramConfiguration;
    private final HttpConfiguration httpConfiguration;

    @Override
    public String getBotUsername() {
        return telegramConfiguration.getUsername();
    }

    @Override
    public String getBotToken() {
        return telegramConfiguration.getToken();
    }

    @Override
    public void onUpdateReceived(Update update) {
        // if you want chatting
        if (update.hasMessage()) {
            Message message = update.getMessage();
            SendMessage response = new SendMessage();
            Long chatId = message.getChatId();
            response.setChatId(String.valueOf(chatId));
            String text = message.getText();
            response.setText(text);
            try {
                execute(response);
                log.info("Sent message \"{}\" to {}", text, chatId);
            } catch (TelegramApiException e) {
                log.error("Failed to send message \"{}\" to {} due to error: {}", text, chatId, e.getMessage());
            }
        }
    }

    public NotificationResponse sendNotification(String message) {
        HttpResponse<String> response = httpConfiguration.send(
                message,
                telegramConfiguration.getToken(),
                telegramConfiguration.getChatId()
        );
        NotificationResponse notificationResponse = new NotificationResponse();
        notificationResponse.setStatusCode(String.valueOf(response.statusCode()));
        notificationResponse.setMessage(response.body());
        return notificationResponse;
    }
}
