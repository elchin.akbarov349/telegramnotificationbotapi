package com.example.bot.constant;

public class TelegramConstant {

    public static final String TELEGRAM_URL ="https://api.telegram.org/";
    public static final String URL_PATH ="/{token}/sendMessage";
    public static final String CHAT_ID ="chat_id";
    public static final String TEXT ="text";
    public static final String BOT ="bot";
}
