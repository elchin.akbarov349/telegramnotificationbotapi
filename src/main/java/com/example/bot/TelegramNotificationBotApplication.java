package com.example.bot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TelegramNotificationBotApplication {

    public static void main(String[] args) {
        SpringApplication.run(TelegramNotificationBotApplication.class, args);
    }

}
